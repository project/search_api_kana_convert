このファイルの内容
---------------------

 * 紹介
 * 要件
 * インストール
 * 設定
 * メンテナー


紹介
---

Search API Kana Convert モジュールは Search API モジュール用に、日本語特有の問題である
文字種別の表記揺れを一定のルールで変換して検索性能を向上させる機能を提供します。

Drupal コアの Search モジュールや Search API の標準機能では、全角で入力された
アルファベットや数字などを半角の文字列で検索することができません。
例えば全角で「ＡＢＣ　１２３」と入力された文字列は半角で「ABC 123」と検索してもヒット
しません。

このモジュールの機能によって検索インデックスの作成時および検索時に上記の様な全角・半角の文字種の
違いを一定のルールで変換し、検索できるようにします。


要件
---

このモジュールは以下のモジュールが必要です:

* [Search API](https://www.drupal.org/project/search_api)

Search API モジュールの設定を行い、コンテンツを検索できるようにしておく必要があります。
Search API モジュールの設定方法などは Search API モジュールのドキュメントなどを参照して
ください。


インストール
----------

* コントリビュートモジュールを一般的な方法でインストールします。詳細については
  https://www.drupal.org/node/1897420 を参照してください。


設定
---

* Search API のインデックスの設定画面にあるプロセッサーの設定をします

  - 「Kana convert」を有効にしてください。

  - 「Preprocess index」および「Preprocess query」で「Kana convert」を実行する順番は
    「Tokenizer」より前にしてください。

  - 「Kana convert」の設定セクションで適宜オプションを設定してください。通常はデフォルトの
    オプションで問題ありません。この機能は PHP の mb_convert_kana 関数を利用しています。
    設定オプションの詳細は mb_convert_kana 関数のドキュメント を参照してください。
    https://www.php.net/manual/ja/function.mb-convert-kana.php

* 検索インデックスをインデックスし直してください。


メンテナー
---------

現在のメンテナー:

* Yutaro Ohno (u7aro): https://www.drupal.org/u/u7aro

このプロジェクトは次の支援を受けています:

* Studio Umi (https://www.studio-umi.jp/)
