CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
The Search API Kana Convert module provides a processor plugin for the Search
API module. It's basically a wrapper of PHP mb_convert_kana function which
allows Japanese character types(zenkaku, hankaku, hiragana, katakana) to be
converted from one another.

USE CASES
------------
- Normalize the data so the same words in different word types can be matched.
  (E.g convert "ABC　１２３" to "ABC 123" or "ひと" to "ヒト".)
- convert full-width space "　" to half-width space " " so the data can be
  properly splitted by tokenizer.

REQUIREMENTS
------------
This module requires the following modules:
* [Search API](https://www.drupal.org/project/search_api)
You must configure the Search API module to be able to search content.
For information on how to set up the Search API module, please refer to the
documentation of the Search API module.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
* In the processor configuration page
(/admin/config/search/search-api/index/[my_index]/processors),
    - Enable "Kana convert".
    - If "Tokenizer" is enabled, place "Kana convert" above Tokenizer in the
      "Processor Order" section.
    - Set the options accordingly in the "Kana convert" settings section. The
      default setup is usually fine. See the mb_convert_kana function
      documentation for more configuration options.
      https://www.php.net/manual/en/function.mb-convert-kana.php
* Reindex your search index.

MAINTAINERS
-----------
Current maintainers:
* Yutaro Ohno (u7aro): https://www.drupal.org/u/u7aro
This project has been sponsored by:
* Studio Umi (https://www.studio-umi.jp/)
