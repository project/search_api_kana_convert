<?php

namespace Drupal\search_api_kana_convert\Plugin\search_api\processor;

use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Convert Japanese 'kana' one from another ('zen-kaku', 'han-kaku' and more).
 *
 * @SearchApiProcessor(
 *   id = "kana_convert",
 *   label = @Translation("Kana convert"),
 *   description = @Translation("Convert Japanese 'kana' one from another ('zen-kaku', 'han-kaku' and more)."),
 *   stages = {
 *     "preprocess_index" = -22,
 *     "preprocess_query" = -22,
 *   }
 * )
 */
class KanaConvert extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();
    $configuration['converts'] = ['a', 's', 'K', 'V'];
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['converts'] = [
      '#title' => 'Kana convert options',
      '#type' => 'checkboxes',
      '#options' => [
        'r' => $this->t('Convert "zen-kaku" alphabets to "han-kaku"'),
        'R' => $this->t('Convert "han-kaku" alphabets to "zen-kaku"'),
        'n' => $this->t('Convert "zen-kaku" numbers to "han-kaku"'),
        'N' => $this->t('Convert "han-kaku" numbers to "zen-kaku"'),
        'a' => $this->t('Convert "zen-kaku" alphabets and numbers to "han-kaku"'),
        'A' => $this->t('Convert "han-kaku" alphabets and numbers to "zen-kaku"'),
        's' => $this->t('Convert "zen-kaku" space to "han-kaku"'),
        'S' => $this->t('Convert "han-kaku" space to "zen-kaku"'),
        'k' => $this->t('Convert "zen-kaku kata-kana" to "han-kaku kata-kana"'),
        'K' => $this->t('Convert "han-kaku kata-kana" to "zen-kaku kata-kana"'),
        'h' => $this->t('Convert "zen-kaku hira-gana" to "han-kaku kata-kana"'),
        'H' => $this->t('Convert "han-kaku kata-kana" to "zen-kaku hira-gana"'),
        'c' => $this->t('Convert "zen-kaku kata-kana" to "zen-kaku hira-gana"'),
        'C' => $this->t('Convert "zen-kaku hira-gana" to "zen-kaku kata-kana"'),
        'V' => $this->t('Collapse voiced sound notation and convert them into a character'),
      ],
      '#default_value' => $this->configuration['converts'],
      '#description' => $this->t('Text will convert with these options. These options conform to the mb_convert_kana() function. See the <a href="https://www.php.net/manual/en/function.mb-convert-kana.php">PHP.net reference page</a> for details.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    $convert_options = array_filter($this->configuration['converts']);
    if ($convert_options) {
      $option = implode('', $convert_options);
      $value = mb_convert_kana($value, $option);
    }
  }

}
